/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const InsuranceContractContract = require('./lib/insurance-contract-contract');
const HealthRecordContract = require('./lib/health-records-contract');


module.exports.InsuranceContractContract = InsuranceContractContract;
module.exports.HealthRecordContract = HealthRecordContract;
module.exports.contracts = [ InsuranceContractContract, HealthRecordContract ];
