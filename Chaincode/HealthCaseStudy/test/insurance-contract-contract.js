/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { InsuranceContractContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('InsuranceContractContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new InsuranceContractContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"insurance contract 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"insurance contract 1002 value"}'));
    });

    describe('#insuranceContractExists', () => {

        it('should return true for a insurance contract', async () => {
            await contract.insuranceContractExists(ctx, '1').should.eventually.be.true;
        });

        it('should return false for a insurance contract that does not exist', async () => {
            await contract.insuranceContractExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createInsuranceContract', () => {

        it('should create a insurance contract', async () => {
            await contract.createInsuranceContract(ctx, '1003', 'insurance contract 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"insurance contract 1003 value"}'));
        });

        it('should throw an error for a insurance contract that already exists', async () => {
            await contract.createInsuranceContract(ctx, '1001', 'myvalue').should.be.rejectedWith(/The insurance contract 1001 already exists/);
        });

    });

    describe('#readInsuranceContract', () => {

        it('should return a insurance contract', async () => {
            await contract.readInsuranceContract(ctx, '1001').should.eventually.deep.equal({ value: 'insurance contract 1001 value' });
        });

        it('should throw an error for a insurance contract that does not exist', async () => {
            await contract.readInsuranceContract(ctx, '1003').should.be.rejectedWith(/The insurance contract 1003 does not exist/);
        });

    });

    describe('#updateInsuranceContract', () => {

        it('should update a insurance contract', async () => {
            await contract.updateInsuranceContract(ctx, '1001', 'insurance contract 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"insurance contract 1001 new value"}'));
        });

        it('should throw an error for a insurance contract that does not exist', async () => {
            await contract.updateInsuranceContract(ctx, '1003', 'insurance contract 1003 new value').should.be.rejectedWith(/The insurance contract 1003 does not exist/);
        });

    });

    describe('#deleteInsuranceContract', () => {

        it('should delete a insurance contract', async () => {
            await contract.deleteInsuranceContract(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a insurance contract that does not exist', async () => {
            await contract.deleteInsuranceContract(ctx, '1003').should.be.rejectedWith(/The insurance contract 1003 does not exist/);
        });

    });

});
