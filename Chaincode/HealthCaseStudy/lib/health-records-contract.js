/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
//const crypto = require('crypto');

async function getCollectionName(ctx) {
    return "CollectionOrder";
}

class HealthRecordsContract extends Contract {

    async healthRecordsExists(ctx, healthRecordsId) {
        const collectionName = "CollectionOrder";
        const data = await ctx.stub.getPrivateDataHash(collectionName, healthRecordsId);
        return (!!data && data.length > 0);
    }
/*
params: patientID, hospitalID, medicalReport, prescription, cost
*/
    async createHealthRecords(ctx, healthRecordsId) {
        const exists = await this.healthRecordsExists(ctx, healthRecordsId);
        if (exists) {
            throw new Error(`The asset health records ${healthRecordsId} already exists`);
        }

        //const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || 
            !transientData.has('patientID') ||
            !transientData.has('hospitalID') ||
            !transientData.has('medicalReport') ||
            !transientData.has('prescription') ||
            !transientData.has('cost') ) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }

        const recordAsset = {};
        
        recordAsset.patientID = transientData.get('patientID').toString();
        recordAsset.hospitalID = transientData.get('hospitalID').toString();
        recordAsset.medicalReport = transientData.get('medicalReport').toString();
        recordAsset.prescription = transientData.get('prescription').toString();
        recordAsset.cost = transientData.get('cost');
        


        // privateAsset.privateValue = transientData.get('privateValue').toString();

        // const collectionName = await getCollectionName(ctx);
        await ctx.stub.putPrivateData('CollectionOrder', healthRecordsId, Buffer.from(JSON.stringify(recordAsset)));
    }

    async readHealthRecords(ctx, healthRecordsId) {
        const exists = await this.healthRecordsExists(ctx, healthRecordsId);
        if (!exists) {
            throw new Error(`The asset health records ${healthRecordsId} does not exist`);
        }
        //let privateDataString;
        // const collectionName = await getCollectionName(ctx);
        const privateData = await ctx.stub.getPrivateData('CollectionOrder', healthRecordsId);
        
        return JSON.parse(privateData.toString());;
    }

    // async updateHealthRecords(ctx, healthRecordsId) {
    //     const exists = await this.healthRecordsExists(ctx, healthRecordsId);
    //     if (!exists) {
    //         throw new Error(`The asset health records ${healthRecordsId} does not exist`);
    //     }
    //     const privateAsset = {};

    //     const transientData = ctx.stub.getTransient();
    //     if (transientData.size === 0 || !transientData.has('privateValue')) {
    //         throw new Error('The privateValue key was not specified in transient data. Please try again.');
    //     }
    //     privateAsset.privateValue = transientData.get('privateValue').toString();

    //     const collectionName = await getCollectionName(ctx);
    //     await ctx.stub.putPrivateData(collectionName, healthRecordsId, Buffer.from(JSON.stringify(privateAsset)));
    // }

    async deleteHealthRecords(ctx, healthRecordsId) {
        const exists = await this.healthRecordsExists(ctx, healthRecordsId);
        if (!exists) {
            throw new Error(`The asset health records ${healthRecordsId} does not exist`);
        }
        const collectionName = await getCollectionName(ctx);
        await ctx.stub.deletePrivateData('CollectionOrder', healthRecordsId);
    }

    // async verifyHealthRecords(ctx, mspid, healthRecordsId, objectToVerify) {

    //     // Convert provided object into a hash
    //     const hashToVerify = crypto.createHash('sha256').update(objectToVerify).digest('hex');
    //     const pdHashBytes = await ctx.stub.getPrivateDataHash(`_implicit_org_${mspid}`, healthRecordsId);
    //     if (pdHashBytes.length === 0) {
    //         throw new Error('No private data hash with the key: ' + healthRecordsId);
    //     }

    //     const actualHash = Buffer.from(pdHashBytes).toString('hex');

    //     // Compare the hash calculated (from object provided) and the hash stored on public ledger
    //     if (hashToVerify === actualHash) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }


}

module.exports = HealthRecordsContract;
