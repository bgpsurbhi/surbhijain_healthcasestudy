/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class InsuranceContractContract extends Contract {

    async insuranceContractExists(ctx, insuranceContractId) {
        const buffer = await ctx.stub.getState(insuranceContractId);
        return (!!buffer && buffer.length > 0);
    }

    async createInsuranceContract(ctx, aadhaarID, name, age, allergies, medicalHistory) {
        const exists = await this.insuranceContractExists(ctx, aadhaarID);
        if (exists) {
            throw new Error(`The insurance contract ${aadhaarID} already exists`);
        }
        const details = { 
            aadhaarID: aadhaarID,
            name: name,
            age: age,
            allergies: allergies,
            medicalHistory: medicalHistory,
        };
        const buffer = Buffer.from(JSON.stringify(details));
        await ctx.stub.putState(aadhaarID, buffer);
    }

    async readInsuranceContract(ctx, aadhaarID) {
        const exists = await this.insuranceContractExists(ctx, aadhaarID);
        if (!exists) {
            throw new Error(`The insurance contract ${aadhaarID} does not exist`);
        }
        const buffer = await ctx.stub.getState(aadhaarID);
        const details = JSON.parse(buffer.toString());
        return details;
    }

    async getInsuranceByRange(ctx, startKey, endKey)
    {
        const iterator = await ctx.stub.getStateByRange(startKey,endKey);
        const result = await this.getAllResult(iterator);
        return JSON.stringify(result);
    }

    // async getAllResult(iterator)
    // {
    //     let allResult = [];
        
    //     for (let res=await iterator.next(); !res.done; res=await iterator.next()){
    //         if(res.value && res.value.value.toString()){
    //             let jsonRes ={};
    //             jsonRes.Key =res.value.key;
    //             jsonRes.Record = res.value.value.toString('utf8');
    //             allResult.push(jsonRes);
    //         }
    //     }
    //     return allResult;
    // }
    async getRecordHistory(ctx, aadhaarID) {
        const iterator = await ctx.stub.getHistoryForKey(aadhaarID);
        const result = await this.getAllResult(iterator);
        return JSON.stringify(result);
    }

    async getAllResult(iterator, isHistory) {
        let allResult = [];

        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};

                if (isHistory && isHistory === true) {
                    jsonRes.output = res.value;
                } else {
                    jsonRes.key = res.value.key;
                    jsonRes.record = res.value.value.toString('utf8');
                }
                allResult.push(jsonRes);
            }
        }
        return allResult;
    }

    async queryAllOrders(ctx) {
        
        const queryString = JSON.stringify({
                selector: {},
                sort: [{age: 'desc'}]
            });

        const iterator = await ctx.stub.getQueryResult((queryString));
        const result = await this.getAllResult(iterator,false)
        //const collectionName = await getCollectionName(ctx);
        // const collectionName = 'CollectionOrder'
        // let resultsIterator = await ctx.stub.getPrivateDataQueryResult(
        //     collectionName,
        //     queryString
        // );

        // let insuranceContractContract = new InsuranceContractContract();
        //let result = await insuranceContractContract.getAllResult(resultsIterator.iterator);
        // NOTE: If the above line of code isn't working please uncomment the below line
        // and un comment getAllResults() function in this file

        // let result = await this.getAllResults(resultsIterator.iterator);

        //const iterator = await ctx.stub.getQueryResult(JSON.stringify)
        return JSON.stringify(result);
    }

    async getAadhaarWithPagination(ctx,pagesize, bookmark)
    {
        const queryString={
            selector:{

            },
        };

        const pages = parseInt(pagesize,10);

        const {iterator, metadata} = await ctx.stub.getQueryResultWithPagination(JSON.stringify(queryString),pages,bookmark);

        const result = await this.getAllResult(iterator,false);

        let results ={};
        results.result =result;
        results.ResponseMetaData ={
            RecordCount : metadata.fetched_records_count,
            Bookmark: metadata.bookmark
        };

        return (JSON.stringify(results));
    }



}

module.exports = InsuranceContractContract;
