
export ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem

export CHANNEL_NAME=autochannel

******************** Approve for Hospital Org ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=HospitalMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode approveformyorg --channelID autochannel \
     --collections-config /opt/gopath/src/github.com/chaincode/HealthCaseStudy/collections.json \
     --name kbaauto --version 1 --sequence 1 --signature-policy "OR('HospitalMSP.peer', 'AadhaarMSP.peer','InsuranceMSP.peer')" \
     --package-id kbaauto_1:a01da92b7a2b27e6733964d0a6d6c79eaaa05bc3d6121b9cc37f9e4dfdcd46cb \
     --tls --cafile $ORDERER_TLS_CA \
     --waitForEvent

******************** Approve for Aadhaar Org ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=AadhaarMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.aadhaar.auto.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/users/Admin@aadhaar.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode approveformyorg --channelID autochannel \
     --name kbaauto --version 1 --sequence 1 --signature-policy "OR('HospitalMSP.peer', 'AadhaarMSP.peer','InsuranceMSP.peer')"\
     --collections-config /opt/gopath/src/github.com/chaincode/HealthCaseStudy/collections.json \
     --package-id kbaauto_1:a01da92b7a2b27e6733964d0a6d6c79eaaa05bc3d6121b9cc37f9e4dfdcd46cb  \
     --tls --cafile $ORDERER_TLS_CA --waitForEvent

******************** Approve for Insurance org ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=InsuranceMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.insurance.auto.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/users/Admin@insurance.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode approveformyorg --channelID autochannel \
     --name kbaauto --version 1 --sequence 1 --signature-policy "OR('HospitalMSP.peer', 'AadhaarMSP.peer','InsuranceMSP.peer')"\
     --collections-config /opt/gopath/src/github.com/chaincode/HealthCaseStudy/collections.json \
     --package-id kbaauto_1:a01da92b7a2b27e6733964d0a6d6c79eaaa05bc3d6121b9cc37f9e4dfdcd46cb  \
     --tls --cafile $ORDERER_TLS_CA --waitForEvent


******************** Check Commit Readiness ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=HospitalMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode checkcommitreadiness --channelID autochannel \
     --name kbaauto --version 1 --sequence 1 --tls --cafile $ORDERER_TLS_CA --output json

******************** Commit Chaincode ***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=HospitalMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode commit -o orderer.auto.com:7050 -C autochannel \
     --name kbaauto --tls --cafile $ORDERER_TLS_CA \
     --peerAddresses peer0.hospital.auto.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     --peerAddresses peer0.aadhaar.auto.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/ca.crt \
     --peerAddresses peer0.insurance.auto.com:11051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/ca.crt \
     --collections-config /opt/gopath/src/github.com/chaincode/HealthCaseStudy/collections.json \
     --version 1 --sequence 1 \
     --signature-policy "OR('HospitalMSP.peer', 'AadhaarMSP.peer','InsuranceMSP.peer')"





******************** Invoke Chaincode As Hospital Org Peer***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=HospitalMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer chaincode invoke -o orderer.auto.com:7050 --tls --cafile $ORDERER_TLS_CA \
     -C autochannel -n kbaauto \
     --peerAddresses peer0.hospital.auto.com:7051 \
     --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     -c '{"function":"createInsuranceContract", "Args":["Aadhaar-001","Surbhi","22","allergy 1","med 1"]}'

******************** Invoke Chaincode As Aadhaar Org Peer***********************

docker exec \
     -e CORE_PEER_LOCALMSPID=AadhaarMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.aadhaar.auto.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/users/Admin@aadhaar.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer chaincode invoke -o orderer.auto.com:7050 --tls --cafile $ORDERER_TLS_CA \
     -C autochannel -n kbaauto \
     --peerAddresses peer0.aadhaar.auto.com:9051 \
     --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/ca.crt \
     -c '{"function":"createInsuranceContract", "Args":["Aadhaar-002","Ishu","23","allergy 12","med 11"]}'
