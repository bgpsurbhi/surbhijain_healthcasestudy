sh createCa.sh

sleep 5

echo "########## Setting required env ############"
export CHANNEL_NAME=autochannel
export IMAGE_TAG=latest
export COMPOSE_PROJECT_NAME=fabricscratch-ca

echo "########## Generate the genesis block ############"
configtxgen -profile OrdererGenesis \
-channelID system-channel -outputBlock \
./channel-artifacts/genesis.block

sleep 2

echo "########## Generate the Channel Transaction ############"
configtxgen -profile AutoChannel \
-outputCreateChannelTx ./channel-artifacts/$CHANNEL_NAME.tx \
-channelID $CHANNEL_NAME

sleep 2

echo "########## Starting the components ############"
docker-compose -f docker/docker-compose-singlepeer.yml up -d

export ORDERER_TLS_CA=`docker exec cli  env | grep ORDERER_TLS_CA | cut -d'=' -f2`

sleep 2

echo "########## Creating the Channel ############"
docker exec cli peer channel create -o orderer.auto.com:7050 \
-c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/peer/config/$CHANNEL_NAME.tx \
--tls --cafile $ORDERER_TLS_CA


sleep 2

echo "########## Joining Hospital Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=HospitalMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining Aadhaar Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=AadhaarMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.aadhaar.auto.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/users/Admin@aadhaar.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining MVD Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=InsuranceMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.insurance.auto.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/users/Admin@insurance.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Generating anchor peer tx for hospital ############"
configtxgen -profile AutoChannel -outputAnchorPeersUpdate ./channel-artifacts/HospitalMSPanchors.tx -channelID autochannel -asOrg HospitalMSP

sleep 2

echo "########## Generating anchor peer tx for aadhaar ############"
configtxgen -profile AutoChannel -outputAnchorPeersUpdate ./channel-artifacts/AadhaarMSPanchors.tx -channelID autochannel -asOrg AadhaarMSP

sleep 2

echo "########## Generating anchor peer tx for aadhaar ############"
configtxgen -profile AutoChannel -outputAnchorPeersUpdate ./channel-artifacts/InsuranceMSPanchors.tx -channelID autochannel -asOrg InsuranceMSP

sleep 2

echo "########## Anchor Peer Update for Hospital ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=HospitalMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.auto.com:7050 -c autochannel -f ./config/HospitalMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for Aadhaar ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=AadhaarMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.aadhaar.auto.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/users/Admin@aadhaar.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.auto.com:7050 -c autochannel -f ./config/AadhaarMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for Insurance ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=InsuranceMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.insurance.auto.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/users/Admin@insurance.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.auto.com:7050 -c autochannel -f ./config/InsuranceMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Package Chaincode ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=HospitalMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode package kbaauto.tar.gz --path /opt/gopath/src/github.com/chaincode/HealthCaseStudy/ --lang node --label kbaauto_1

sleep 2

echo "##########  Install Chaincode on Hospital peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=HospitalMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install kbaauto.tar.gz


sleep 2

echo "##########  Install Chaincode on Aadhaar peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=AadhaarMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.aadhaar.auto.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/aadhaar.auto.com/users/Admin@aadhaar.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install kbaauto.tar.gz

sleep 2

echo "##########  Install Chaincode on Insurance peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=InsuranceMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.insurance.auto.com:11051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/insurance.auto.com/users/Admin@insurance.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install kbaauto.tar.gz


echo "##########  Copy the above package ID for next steps, follow the approveCommit.txt ############"
