#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
version: '2'

networks:
  automobile:

services:
  couchdbHospitalPeer0:
    container_name: couchdbHospitalPeer0
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=peer0.Hospital
      - COUCHDB_PASSWORD=password
    ports:
      - 5984:5984
    networks:
      - automobile

  couchdbAadhaarPeer0:
    container_name: couchdbAadhaarPeer0
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=peer0.Aadhaar
      - COUCHDB_PASSWORD=password
    ports:
      - 7984:5984
    networks:
      - automobile

  couchdbInsurancePeer0:
    container_name: couchdbInsurancePeer0
    image: hyperledger/fabric-couchdb
    environment:
      - COUCHDB_USER=peer0.Insurance
      - COUCHDB_PASSWORD=password
    ports:
      - 9984:5984
    networks:
      - automobile

  orderer.auto.com:
    container_name: orderer.auto.com
    image: hyperledger/fabric-orderer:2.2.3
    environment:
      - FABRIC_LOGGING_SPEC=INFO
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_LISTENPORT=7050
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
      - ORDERER_GENERAL_CLUSTER_CLIENTCERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_CLUSTER_CLIENTPRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_CLUSTER_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/orderer
    command: orderer
    ports:
      - 7050:7050
    volumes:
      - ../channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
      - ../organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp:/var/hyperledger/orderer/msp
      - ../organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls:/var/hyperledger/orderer/tls
    networks:
      - automobile

  peer0.hospital.auto.com:
    container_name: peer0.hospital.auto.com
    image: hyperledger/fabric-peer:2.2.3
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer0.hospital.auto.com
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_LISTENADDRESS=0.0.0.0:7051
      - CORE_PEER_CHAINCODEADDRESS=peer0.hospital.auto.com:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.hospital.auto.com:7051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.hospital.auto.com:7051
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_CHAINCODE_LOGGING_LEVEL=INFO
      - CORE_PEER_LOCALMSPID=HospitalMSP
      - CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_automobile
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdbHospitalPeer0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=peer0.Hospital
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    ports:
      - 7051:7051
      - 7053:7053
    volumes:
      - /var/run/:/host/var/run/
      - ../organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/msp:/etc/hyperledger/fabric/msp
      - ../organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls:/etc/hyperledger/fabric/tls
      
    depends_on:
      - orderer.auto.com
      - couchdbHospitalPeer0  
    networks:
      - automobile

  peer0.aadhaar.auto.com:
    container_name: peer0.aadhaar.auto.com
    image: hyperledger/fabric-peer:2.2.3
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer0.aadhaar.auto.com
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_LISTENADDRESS=0.0.0.0:9051
      - CORE_PEER_CHAINCODEADDRESS=peer0.aadhaar.auto.com:9052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:9052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.aadhaar.auto.com:9051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.aadhaar.auto.com:9051
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_CHAINCODE_LOGGING_LEVEL=INFO
      - CORE_PEER_LOCALMSPID=AadhaarMSP
      - CORE_PEER_ADDRESS=peer0.aadhaar.auto.com:9051
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_automobile
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdbAadhaarPeer0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=peer0.Aadhaar
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    ports:
      - 9051:9051
      - 9053:9053
    volumes:
      - /var/run/:/host/var/run/
      - ../organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/msp:/etc/hyperledger/fabric/msp
      - ../organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls:/etc/hyperledger/fabric/tls
      
    depends_on:
      - orderer.auto.com
      - couchdbAadhaarPeer0  
    networks:
      - automobile 

  peer0.insurance.auto.com:
    container_name: peer0.insurance.auto.com
    image: hyperledger/fabric-peer:2.2.3
    environment:
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - CORE_PEER_ID=peer0.insurance.auto.com
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_LISTENADDRESS=0.0.0.0:11051
      - CORE_PEER_CHAINCODEADDRESS=peer0.insurance.auto.com:11052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:11052
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer0.insurance.auto.com:11051
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.insurance.auto.com:11051
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      - CORE_CHAINCODE_LOGGING_LEVEL=INFO
      - CORE_PEER_LOCALMSPID=InsuranceMSP
      - CORE_PEER_ADDRESS=peer0.insurance.auto.com:11051
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_automobile
      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdbInsurancePeer0:5984
      - CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=peer0.Insurance
      - CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=password
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: peer node start
    ports:
      - 11051:11051
      - 11053:11053
    volumes:
      - /var/run/:/host/var/run/
      - ../organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/msp:/etc/hyperledger/fabric/msp
      - ../organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls:/etc/hyperledger/fabric/tls
      
    depends_on:
      - orderer.auto.com
      - couchdbInsurancePeer0  
    networks:
      - automobile    

  cli:
    container_name: cli
    image: hyperledger/fabric-tools:2.2.3
    tty: true
    environment:
      - GOPATH=/opt/gopath
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_ID=cli
      - CORE_PEER_ADDRESS=peer0.hospital.auto.com:7051
      - CORE_PEER_LOCALMSPID=HospitalMSP
      - CORE_CHAINCODE_KEEPALIVE=10
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp
      - ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: /bin/bash
    volumes:
      - /var/run/:/host/var/run/
      - ../../Chaincode/:/opt/gopath/src/github.com/chaincode/
      - ../organizations:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
      - ../channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/config/
    networks:
        - automobile
    depends_on:
      - orderer.auto.com
      - peer0.hospital.auto.com
      - peer0.aadhaar.auto.com
      - peer0.insurance.auto.com

