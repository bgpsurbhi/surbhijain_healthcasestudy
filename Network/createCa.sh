echo "##########Strating the docker-compose-ca############"
export COMPOSE_PROJECT_NAME=fabricscratch-ca
docker-compose -f ./docker/docker-compose-ca.yml up -d 

echo "##########Creating the folder structure for CA############"

mkdir -p organizations/peerOrganizations/auto.com/hospital.auto.com/
mkdir -p organizations/peerOrganizations/auto.com/aadhaar.auto.com/
mkdir -p organizations/peerOrganizations/auto.com/insurance.auto.com/

echo "##########Setting PATH for Fabric CA client############"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/
sleep 2

echo "########## Enroll Manufacturer ca admin############"

fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-hospital --tls.certfiles ${PWD}/organizations/fabric-ca/hospital/tls-cert.pem

sleep 5

echo "########## Create config.yaml for Manufacturer ############"

echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-hospital.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-hospital.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-hospital.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-hospital.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/msp/config.yaml

echo "########## Register hospital peer ############"
fabric-ca-client register --caname ca-hospital --id.name peer0hospital --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/hospital/tls-cert.pem
sleep 5

echo "########## Register hospital user ############"

fabric-ca-client register --caname ca-hospital --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/hospital/tls-cert.pem
sleep 5

echo "########## Register hospital Org admin ############"

fabric-ca-client register --caname ca-hospital --id.name hospitaladmin --id.secret hospitaladminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/hospital/tls-cert.pem
sleep 5

echo "########## Generate Manufacturer peer MSP ############"

fabric-ca-client enroll -u https://peer0hospital:peer0pw@localhost:7054 --caname ca-hospital -M ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/msp --csr.hosts peer0.hospital.auto.com --tls.certfiles ${PWD}/organizations/fabric-ca/hospital/tls-cert.pem
sleep 5

echo "########## Generate Manufacturer Peer tls cert ############"

fabric-ca-client enroll -u https://peer0hospital:peer0pw@localhost:7054 --caname ca-hospital -M ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls --enrollment.profile tls --csr.hosts peer0.hospital.auto.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/hospital/tls-cert.pem
sleep 5

echo "########## Organizing the folders ############"
echo "########## Copy the certificate files ############"

cp organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/tlscacerts/* organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/ca.crt
cp organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/signcerts/* organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.crt
cp organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/keystore/* organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/tlsca
cp ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/tlsca/tls-localhost-7054-ca-hospital.pem

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/ca
cp ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/ca/localhost-7054-ca-hospital.pem

cp organizations/peerOrganizations/auto.com/hospital.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/hospital.auto.com/peers/peer0.hospital.auto.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-hospital -M ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/users/User1@hospital.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/hospital/tls-cert.pem

sleep 5

cp organizations/peerOrganizations/auto.com/hospital.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/hospital.auto.com/users/User1@hospital.auto.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://hospitaladmin:hospitaladminpw@localhost:7054 --caname ca-hospital -M ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/hospital/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/auto.com/hospital.auto.com/users/Admin@hospital.auto.com/msp/config.yaml

echo "============================================================End of Manufacturer =================================================================================================="

echo "================================================================== Dealer =================================================================================================="


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/

echo "enroll Dealer ca"
echo "======================"

fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-aadhaar --tls.certfiles ${PWD}/organizations/fabric-ca/aadhaar/tls-cert.pem

sleep 5




echo "Create config.yaml for Dealer" 
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-aadhaar.pem 
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-aadhaar.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-aadhaar.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-aadhaar.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/msp/config.yaml

echo "Register aadhaar peer"
echo "=========================="
fabric-ca-client register --caname ca-aadhaar --id.name peer0aadhaar --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/aadhaar/tls-cert.pem
sleep 5

echo "Register aadhaar user"
echo "=========================="
fabric-ca-client register --caname ca-aadhaar --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/aadhaar/tls-cert.pem
sleep 5

echo "Register aadhaar Org admin"
echo "=========================="
fabric-ca-client register --caname ca-aadhaar --id.name aadhaaradmin --id.secret aadhaaradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/aadhaar/tls-cert.pem
sleep 5

echo "Generate Dealer peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0aadhaar:peer0pw@localhost:8054 --caname ca-aadhaar -M ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/msp --csr.hosts peer0.aadhaar.auto.com --tls.certfiles ${PWD}/organizations/fabric-ca/aadhaar/tls-cert.pem
sleep 5

echo "Generate Dealer Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0aadhaar:peer0pw@localhost:8054 --caname ca-aadhaar -M ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls --enrollment.profile tls --csr.hosts peer0.aadhaar.auto.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/aadhaar/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/tlscacerts/* organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/ca.crt
cp organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/signcerts/* organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.crt
cp organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/keystore/* organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/tlsca
cp ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/tlsca/tls-localhost-8054-ca-aadhaar.pem

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/ca
cp ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/ca/localhost-8054-ca-aadhaar.pem

cp organizations/peerOrganizations/auto.com/aadhaar.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers/peer0.aadhaar.auto.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-aadhaar -M ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/users/User1@aadhaar.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/aadhaar/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/auto.com/aadhaar.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/aadhaar.auto.com/users/User1@aadhaar.auto.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://aadhaaradmin:aadhaaradminpw@localhost:8054 --caname ca-aadhaar -M ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/users/Admin@aadhaar.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/aadhaar/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/auto.com/aadhaar.auto.com/users/Admin@aadhaar.auto.com/msp/config.yaml


echo "==========================================================End of Dealer ========================================================================================================"

echo "========================================================== Mvd ================================================================================================================="
echo "enroll Mvd ca "
echo "======================"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9054 --caname ca-insurance --tls.certfiles ${PWD}/organizations/fabric-ca/insurance/tls-cert.pem
sleep 5





echo "Create config.yaml for Mvd"
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-insurance.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-insurance.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-insurance.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-insurance.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/msp/config.yaml

echo "Register insurance peer"
echo "=========================="
fabric-ca-client register --caname ca-insurance --id.name peer0insurance --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/insurance/tls-cert.pem
sleep 5

echo "Register insurance user"
echo "=========================="
fabric-ca-client register --caname ca-insurance --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/insurance/tls-cert.pem
sleep 5

echo "Register insurance Org admin"
echo "=========================="
fabric-ca-client register --caname ca-insurance --id.name insuranceadmin --id.secret insuranceadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/insurance/tls-cert.pem
sleep 5

echo "Generate Mvd peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0insurance:peer0pw@localhost:9054 --caname ca-insurance -M ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/msp --csr.hosts peer0.insurance.auto.com --tls.certfiles ${PWD}/organizations/fabric-ca/insurance/tls-cert.pem
sleep 5

echo "Generate Mvd Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0insurance:peer0pw@localhost:9054 --caname ca-insurance -M ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls --enrollment.profile tls --csr.hosts peer0.insurance.auto.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/insurance/tls-cert.pem
sleep 5


echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/tlscacerts/* organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/ca.crt
cp organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/signcerts/* organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.crt
cp organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/keystore/* organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/tlsca
cp ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/tlsca/tls-localhost-9054-ca-insurance.pem

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/ca
cp ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/ca/localhost-9054-ca-insurance.pem

cp organizations/peerOrganizations/auto.com/insurance.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/insurance.auto.com/peers/peer0.insurance.auto.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:9054 --caname ca-insurance -M ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/users/User1@insurance.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/insurance/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/auto.com/insurance.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/insurance.auto.com/users/User1@insurance.auto.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://insuranceadmin:insuranceadminpw@localhost:9054 --caname ca-insurance -M ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/users/Admin@insurance.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/insurance/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/auto.com/insurance.auto.com/users/Admin@insurance.auto.com/msp/config.yaml

echo "==========================================================End of Mvd ========================================================================================================================"

echo "==========================================================Orderer Config ========================================================================================================================"


echo "enroll Orderer ca"
echo "================="

mkdir -p organizations/ordererOrganizations/auto.com/orderer.auto.com/


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9052 --caname ca-orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5
echo "Create config.yaml for Mvd "
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/config.yaml

echo "Register orderer"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name orderer0 --id.secret orderer0pw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer Org admin"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name ordereradmin --id.secret ordereradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Generate Orderer MSP"
echo "=============================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp --csr.hosts orderer.auto.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp/config.yaml

echo "Generate Orderer TLS certificates"
echo "==================================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls --enrollment.profile tls --csr.hosts orderer.auto.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"
echo "Copy the certificate files"
echo "=========================="

  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/ca.crt
  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/server.crt
  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/keystore/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/server.key

  mkdir -p ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem

  mkdir -p ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem

  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp/config.yaml

echo "Generate Orderer admin MSP"
echo "=========================="
  fabric-ca-client enroll -u https://ordereradmin:ordereradminpw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/users/Admin@auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/users/Admin@auto.com/msp/config.yaml

echo "=========================================================End of Mvd ========================================================================================================================"







