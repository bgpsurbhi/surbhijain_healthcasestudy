#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s/\${ORGMSP}/$6/" \
        ccp-template.json
}

ORG=hospital
P0PORT=7051
CAPORT=7054
PEERPEM=../organizations/peerOrganizations/auto.com/hospital.auto.com/tlsca/tls-localhost-7054-ca-hospital.pem
CAPEM=../organizations/peerOrganizations/auto.com/hospital.auto.com/ca/localhost-7054-ca-hospital.pem
ORGMSP=Manufacturer

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-hospital.json

ORG=aadhaar
P0PORT=9051
CAPORT=8054
PEERPEM=../organizations/peerOrganizations/auto.com/aadhaar.auto.com/tlsca/tls-localhost-8054-ca-aadhaar.pem
CAPEM=../organizations/peerOrganizations/auto.com/aadhaar.auto.com/ca/localhost-8054-ca-aadhaar.pem
ORGMSP=Dealer

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-aadhaar.json

ORG=insurance
P0PORT=11051
CAPORT=9054
PEERPEM=../organizations/peerOrganizations/auto.com/insurance.auto.com/tlsca/tls-localhost-9054-ca-insurance.pem
CAPEM=../organizations/peerOrganizations/auto.com/insurance.auto.com/ca/localhost-9054-ca-insurance.pem
ORGMSP=Mvd

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-insurance.json
