#!/bin/bash


sudo rm -rf \
    ./organizations/fabric-ca/aadhaar/msp \
    ./organizations/fabric-ca/aadhaar/ca-cert.pem \
    ./organizations/fabric-ca/aadhaar/fabric-ca-server.db \
    ./organizations/fabric-ca/aadhaar/IssuerPublicKey \
    ./organizations/fabric-ca/aadhaar/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/aadhaar/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/hospital/msp \
    ./organizations/fabric-ca/hospital/ca-cert.pem \
    ./organizations/fabric-ca/hospital/fabric-ca-server.db \
    ./organizations/fabric-ca/hospital/IssuerPublicKey \
    ./organizations/fabric-ca/hospital/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/hospital/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/insurance/msp \
    ./organizations/fabric-ca/insurance/ca-cert.pem \
    ./organizations/fabric-ca/insurance/fabric-ca-server.db \
    ./organizations/fabric-ca/insurance/IssuerPublicKey \
    ./organizations/fabric-ca/insurance/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/insurance/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/orderer/msp \
    ./organizations/fabric-ca/orderer/ca-cert.pem \
    ./organizations/fabric-ca/orderer/fabric-ca-server.db \
    ./organizations/fabric-ca/orderer/IssuerPublicKey \
    ./organizations/fabric-ca/orderer/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/orderer/tls-cert.pem

sudo rm -rf \
    ./organizations/ordererOrganizations/auto.com/orderer.auto.com/msp \
    ./organizations/ordererOrganizations/auto.com/orderer.auto.com/fabric-ca-client-config.yaml \
    ./organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers \
    ./organizations/ordererOrganizations/auto.com/orderer.auto.com/users
    
sudo rm -rf \
    ./organizations/peerOrganizations/auto.com/hospital.auto.com/ca \
    ./organizations/peerOrganizations/auto.com/hospital.auto.com/msp \
    ./organizations/peerOrganizations/auto.com/hospital.auto.com/peers \
    ./organizations/peerOrganizations/auto.com/hospital.auto.com/tlsca \
    ./organizations/peerOrganizations/auto.com/hospital.auto.com/users \
    ./organizations/peerOrganizations/auto.com/hospital.auto.com/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/auto.com/aadhaar.auto.com/ca \
    ./organizations/peerOrganizations/auto.com/aadhaar.auto.com/msp \
    ./organizations/peerOrganizations/auto.com/aadhaar.auto.com/peers \
    ./organizations/peerOrganizations/auto.com/aadhaar.auto.com/tlsca \
    ./organizations/peerOrganizations/auto.com/aadhaar.auto.com/users \
    ./organizations/peerOrganizations/auto.com/aadhaar.auto.com/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/auto.com/insurance.auto.com/ca \
    ./organizations/peerOrganizations/auto.com/insurance.auto.com/msp \
    ./organizations/peerOrganizations/auto.com/insurance.auto.com/peers \
    ./organizations/peerOrganizations/auto.com/insurance.auto.com/tlsca \
    ./organizations/peerOrganizations/auto.com/insurance.auto.com/users \
    ./organizations/peerOrganizations/auto.com/insurance.auto.com/fabric-ca-client-config.yaml