var express = require('express');
var router = express.Router();
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path')
const {clientApplication} = require('./client');
const Joi = require('joi')
var jwt = require('jsonwebtoken');
var {applicationSecret} = require('./utils')


router.get('/', function(req, res, next) {
    res.render('index', { title: 'Hospital Dashboard' });
})


/* GET home page. */
router.post('/record',async function(req, res, next) {
 
    let HospitalClient = new clientApplication();

    //adhaarID,name,age,allergies,medicalHistory, manufacturer
    var userName = req.body.userName;
    var txName = 'createInsuranceContract';
    var adhaarID = req.body.adhaarID;
    var name = req.body.name;
    var age = req.body.age;
    var allergies = req.body.allergies;
    var medicalHistory = req.body.medicalHistory;
     
    var decoded = jwtVerify(req)
    
    if(decoded.userName === userName) {

    

    const schema = Joi.object({
        userName: Joi.string().required(),
        adhaarID: Joi.string().required(),
        name: Joi.string().required(),
        age: Joi.string().required(),
        allergies: Joi.string().required(),
        medicalHistory: Joi.string().required(),
        });
      
      const { error } = schema.validate(req.body);
      if(error) {
        res.status(500).send({ "status": "false", "message": `${error.message}` });
    } else {
    HospitalClient.generatedAndSubmitTxn(
        "hospital",
        userName,
        "autochannel", 
        "kbaauto",
        "InsuranceContractContract",
        txName,
        adhaarID,name,age,allergies,medicalHistory,manufacturerName
      ).then(message => {
          console.log("Message is $$$$",message)
          res.status(200).send({status:true, message: "Added Car"})
        }
      )
      .catch(error =>{
        console.log("Some error Occured $$$$###", error)
        res.status(500).send({status:false, error:`Failed to Add`,message:`${error}`})
      });
    }
} else {
    res.status(401).send("Unauthorized request")
}

});

router.get('/record', async function(req, res, next) {
    
    var adhaarID = req.query.id;
    var userName = req.query.userName
    let HospitalClient = new clientApplication();
    
    var decoded = jwtVerify(req)

    if(decoded.userName === userName) {
    HospitalClient.generatedAndEvaluateTxn( 
        "hospital",
        userName,
        "autochannel", 
        "kbaauto",
        "InsuranceContractContract",
        "readInsuranceContract", adhaarID)
        .then(message => {
          
          res.status(200).send({status:true, Cardata : message.toString() });
        }).catch(error =>{
         
          res.status(500).send({status: false, error:`Failed to Add`,message:`${error}`})
        });
    } else {
        res.status(401).send("Unauthorized request")
    }
})

//For UI
router.post('/addRecord',async function(req, res, next) {
 
    let HospitalClient = new clientApplication();

    //adhaarID,name,age,allergies,medicalHistory, manufacturer
    var userName = req.body.userName;
    var txName = 'createInsuranceContract';
    var adhaarID = req.body.adhaarID;
    var name = req.body.name;
    var age = req.body.age;
    var allergies = req.body.allergies;
    var medicalHistory = req.body.medicalHistory;
    
    const schema = Joi.object({
        userName: Joi.string().required(),
        adhaarID: Joi.string().required(),
        name: Joi.string().required(),
        age: Joi.string().required(),
        allergies: Joi.string().required(),
        medicalHistory: Joi.string().required(),
        });
      
      const { error } = schema.validate(req.body);
      if(error) {
        res.status(500).send({ "status": "false", "message": `${error.message}` });
    } else {
      console.log("here");
    HospitalClient.generatedAndSubmitTxn(
        "hospital",
        userName,
        "autochannel", 
        "kbaauto",
        "InsuranceContractContract",
        txName,
        adhaarID,name,age,allergies,medicalHistory
      ).then(message => {
          console.log("Message is $$$$",message)
          res.status(200).send({status:true, message: "Added Record"})
        }
      )
      .catch(error =>{
        console.log("Some error Occured $$$$###", error)
        res.status(500).send({status:false, error:`Failed to Add`,message:`${error}`})
      });
    }


});




const jwtVerify = (req) => {
    var token = req.headers.authorization;
    var jwtToken = token ? token.split(' ')[1] : ''
    try {
        var decoded = jwt.verify(jwtToken, applicationSecret);
        return decoded
      } catch(err) {

        return "error"
      }

}
//http://localhost:3000/record?id=01&&userName=appUser
    

module.exports = router;
