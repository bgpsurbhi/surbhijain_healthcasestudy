function HealthWriteData(){
    event.preventDefault();
    // aadhaarID, name, age, allergies, medicalHistory 
    const adhaarID = document.getElementById('adhaarID').value;
    const name = document.getElementById('name').value;
    const age = document.getElementById('age').value;
    const allergies = document.getElementById('allergies').value;
    const medicalHistory = document.getElementById('medicalHistory').value;
    const userName = document.getElementById('userName').value;
    console.log(adhaarID+name+age+allergies+medicalHistory+userName);

    if (adhaarID.length==0||name.length==0||age.length==0||allergies.length==0||medicalHistory.length==0||userName.length==0) {
        alert("You Missed Something")
        }
    else{
        fetch('/addRecord',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({adhaarID: adhaarID, name: name, age: age, allergies: allergies, medicalHistory: medicalHistory, userName: userName})
        })
        .then(function(response){
            if(response.status == 200) {
                alert("Added Record")
            } else {
                alert("Something Went wrong")
            }

        })
        .catch(function(error){
            alert("Something went wrong")
        })    
    }
}
